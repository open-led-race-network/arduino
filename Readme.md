﻿# __OUTDATED__

## 2023-08-19
The Firmware for "Standalone OLR" now manage "Network-enabled OLR (Relay Race)" as well.

```
The Network features, implemented in the version of the Arduino firmware mantained in this Gitlab Project,
were integrated in the "main" Arduino OLR Firmware.

Current implementation of the Network Client uses this new unified Arduino Firmware
      
```
Unified version: https://gitlab.com/open-led-race/olr-arduino


# Obsolete
```
# Open LED Race / Network edition - Arduino software

A Network-enabled version of the original software by:
 - gbarbarov@singulardevices.com  for Arduino day Seville 2019
    An minimalist cars race for LED strip
      https://github.com/gbarbarov/led-race

Authors:
 - Angel Maldonado - gitlab: @angeljmc
 - Luca Borsari - gitlab: @lucabuka

see the Project home page for documentation
   http://www.openrelayrace.org/


**This is the Arduino part of the Open LED Race Network bundle 
composed by this softare plus the Network Client **
```

